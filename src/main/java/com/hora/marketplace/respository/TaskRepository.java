package com.hora.marketplace.respository;

import com.hora.marketplace.model.Task;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends MongoRepository<Task, String> {
    List<Task> findByCategory(String category);

    @Query("{'consumerId' : ?0 , 'completed' : ?1}")
    List<Task> findByConsumerIdAndCompleted(String consumerId, boolean completed);

    List<Task> findByWorkerId(String workerId);

    List<Task> findByCompleted(boolean completed);
}
