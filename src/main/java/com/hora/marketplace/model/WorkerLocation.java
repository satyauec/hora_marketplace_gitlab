package com.hora.marketplace.model;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class WorkerLocation {
    private double latitude;
    private double logitude;
}
