package com.hora.marketplace.model;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class AcceptRequest {
    private String taskId;
    private String agentName;
    private String status;
    private String agentId;
}
