package com.hora.marketplace.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Getter @Setter
@Document(collection = "tasks")
public class Task implements Comparable<Task> {

    @Id
    private String taskId;
    private String name;
    private String description;
    private String category;
    private String location;
    private Date date= new Date();
    private boolean completed;
    private String consumerId;
    private String consumerName;
    private String workerId;
    private String workerName;
    private String workerRating;
    private String consumerRating;
    private double latitude;
    private double longitude;
    private double distance;
    private String status;

    @Override
    public int compareTo(Task o) {
        if(this.getDistance()-o.getDistance()>0){
            return 0;
        }else{
            return -1;
        }
    }
}
