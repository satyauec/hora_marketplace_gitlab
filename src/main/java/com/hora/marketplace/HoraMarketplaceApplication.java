package com.hora.marketplace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HoraMarketplaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HoraMarketplaceApplication.class, args);
	}

}
