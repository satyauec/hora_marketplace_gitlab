package com.hora.marketplace.controller;


import com.hora.marketplace.model.AcceptRequest;
import com.hora.marketplace.model.Task;
import com.hora.marketplace.model.WorkerLocation;
import com.hora.marketplace.respository.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Sort;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/mp/worker")
public class WorkerController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private final TaskRepository taskRepository=null;

    @RequestMapping(value = "/task/list", method = RequestMethod.GET)
    public List<Task> listTask() {
        log.info("All the tasks");
        //Sort sort = new Sort("date",Sort.Direction.DESC);
        Sort sort = Sort.by(Sort.Direction.DESC,"date");
        return taskRepository.findAll(sort);
    }

    @RequestMapping(value = "/task/list/category", method = RequestMethod.GET)
    public List<Task> listTask(@RequestParam String category) {
        log.info("All the tasks in a category");
        return taskRepository.findByCategory(category);
    }

    @RequestMapping(value = "/task/list/workerId", method = RequestMethod.GET)
    public List<Task> listWorkerTask(@RequestParam String workerId ) {
        log.info("All completed Task");
        return taskRepository.findByWorkerId(workerId);
    }

    @RequestMapping(value = "/rate/consumer", method = RequestMethod.POST)
    public Task rateWorker(@RequestBody Task task) {
        log.info("Rating worker");

        Optional<Task> taskDetails = taskRepository.findById(task.getTaskId());

        taskDetails.get().setConsumerRating(task.getConsumerRating());

        return taskRepository.save(taskDetails.get());
    }


    @RequestMapping(value = "/task/sort/distance", method = RequestMethod.POST)
    public List<Task> sortTasksByDistance(@RequestBody WorkerLocation wl) {
        log.info("Sorting by distance");

        List<Task> list=taskRepository.findByCompleted(false);
        double lat1=wl.getLatitude();
        double lon1=wl.getLogitude();
        for(Task t:list){
            double lat2=t.getLatitude();
            double lon2=t.getLongitude();
            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) +
                    Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;
            t.setDistance(dist);
        }
        Collections.sort(list);
        return list;
    }

    @RequestMapping(value = "/task/accept", method = RequestMethod.POST)
    public Task rateWorker(@RequestBody AcceptRequest acceptRequest) {
        log.info("Rating worker");

        Optional<Task> taskDetails = taskRepository.findById(acceptRequest.getTaskId());

        taskDetails.get().setStatus(acceptRequest.getStatus());
        taskDetails.get().setWorkerName(acceptRequest.getAgentName());
        taskDetails.get().setWorkerId(acceptRequest.getAgentId());
        return taskRepository.save(taskDetails.get());
    }
}
