package com.hora.marketplace.controller;

import com.hora.marketplace.model.Task;
import com.hora.marketplace.model.WorkerLocation;
import com.hora.marketplace.respository.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/mp/consumer")
public class ConsumerController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private final TaskRepository taskRepository=null;

    @RequestMapping(value = "/task/create", method = RequestMethod.POST)
    public Task addNewTask(@RequestBody Task task) {
        log.info("Saving Task");
        return taskRepository.save(task);
    }

    @RequestMapping(value = "/task/list/completed", method = RequestMethod.GET)
    public List<Task> listTask(@RequestParam String consumerId, @RequestParam boolean completed ) {
        log.info("All completed Task");
        return taskRepository.findByConsumerIdAndCompleted(consumerId,completed);
    }

    @RequestMapping(value = "/rate/worker", method = RequestMethod.POST)
    public Task rateWorker(@RequestBody Task task) {
        log.info("Rating worker");

        Optional<Task> taskDetails = taskRepository.findById(task.getTaskId());

        taskDetails.get().setWorkerRating(task.getWorkerRating());

        return taskRepository.save(taskDetails.get());
    }


}
